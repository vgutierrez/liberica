/* Copyright (C) 2018-present, Facebook, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package katran

import (
	"context"
	"log"

	"google.golang.org/grpc"
	lb_katran "wikimedia.org/operations/software/liberica/pkg/protos"
)

const (
	IPPROTO_TCP = 6
	IPPROTO_UDP = 17
	NO_SPORT    = 1
	NO_LRU      = 2
	QUIC_VIP    = 4
	DPORT_HASH  = 8
	LOCAL_VIP   = 32

	LOCAL_REAL = 2
)

const (
	ADD_VIP = iota
	DEL_VIP
	MODIFY_VIP
)

var (
	vipFlagTranslationTable = map[string]int64{
		"NO_SPORT":   NO_SPORT,
		"NO_LRU":     NO_LRU,
		"QUIC_VIP":   QUIC_VIP,
		"DPORT_HASH": DPORT_HASH,
		"LOCAL_VIP":  LOCAL_VIP,
	}
	realFlagTranslationTable = map[string]int32{
		"LOCAL_REAL": LOCAL_REAL,
	}
)

type KatranClient struct {
	client lb_katran.KatranServiceClient
}

func (kc *KatranClient) Init(serverAddr string) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	conn, err := grpc.Dial(serverAddr, opts...)
	if err != nil {
		log.Fatalf("Can't connect to local katran server! err is %v\n", err)
	}
	kc.client = lb_katran.NewKatranServiceClient(conn)
}

func (kc *KatranClient) GetAllVips(ctx context.Context) (lb_katran.Vips, error) {
	vips, err := kc.client.GetAllVips(ctx, &lb_katran.Empty{})
	if err != nil {
		return lb_katran.Vips{}, err
	}
	return *vips, nil
}

func (kc *KatranClient) GetStatsForVip(ctx context.Context, vip *lb_katran.Vip) (lb_katran.Stats, error) {
	stats, err := kc.client.GetStatsForVip(ctx, vip)
	if err != nil {
		return lb_katran.Stats{}, err
	}
	return *stats, nil
}
