/*
Copyright © 2022-2023 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	HttpCheck = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_healthcheck_httpcheck_responses_total",
			Help: "Number of HTTP responses received by the HTTP Check",
		},
		[]string{"status_code", "service", "url", "mark"},
	)
	HttpCheckSeconds = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_healthcheck_httpcheck_duration_seconds",
			Help: "Duration of the HTTP(S) request in seconds",
		},
		[]string{"status_code", "service", "url", "mark"},
	)

	HealthcheckResult = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "liberica_healthcheck_result",
			Help: "Healthcheck result for a specific real server",
		},
		[]string{"service", "healthcheck", "mark"},
	)

	HealthcheckExecutions = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "liberica_healthcheck_executions",
			Help: "Number of executions for a specific healthcheck and real server",
		},
		[]string{"service", "healthcheck", "mark", "result"},
	)
)
