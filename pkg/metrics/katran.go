/*
Copyright © 2023 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package metrics

import (
	"context"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"wikimedia.org/operations/software/liberica/pkg/katran"
)

var (
	KatranPacketsDesc = prometheus.NewDesc(
		"liberica_katran_packets_total",
		"Number of total incoming packets per VIP",
		[]string{"vip"}, nil,
	)
	KatranBytesDesc = prometheus.NewDesc(
		"liberica_katran_bytes_total",
		"Number of total incoming bytes per VIP",
		[]string{"vip"}, nil,
	)
	// https://prometheus.io/docs/instrumenting/writing_exporters/#failed-scrapes
	KatranUpDesc = prometheus.NewDesc(
		"liberica_katran_up",
		"Reports if the current scrape was successful or not",
		nil, nil,
	)
)

type KatranMetricCollector struct {
	client katran.KatranClient
}

func NewKatranMetricCollector(address string) *KatranMetricCollector {
	var kc katran.KatranClient
	kc.Init(address)
	kmc := &KatranMetricCollector{client: kc}
	return kmc
}

func (kmc KatranMetricCollector) GetStatsByVip() (
	packets, bytes map[string]float64, err error) {
	ctx := context.Background()
	vips, err := kmc.client.GetAllVips(ctx)
	if err != nil {
		return nil, nil, err
	}

	packets = make(map[string]float64)
	bytes = make(map[string]float64)

	for _, vip := range vips.Vips {
		key := strings.Join([]string{
			vip.Address, strconv.Itoa(int(vip.Port)),
			strconv.Itoa(int(vip.Protocol))}, ":")
		stats, err := kmc.client.GetStatsForVip(ctx, vip)
		if err != nil {
			return nil, nil, err
		}
		packets[key] = float64(stats.V1)
		bytes[key] = float64(stats.V2)
	}
	return packets, bytes, nil
}

func (kmc KatranMetricCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(kmc, ch)
}

func (kmc KatranMetricCollector) Collect(ch chan<- prometheus.Metric) {
	packetsByVip, bytesByVip, err := kmc.GetStatsByVip()
	successful := float64(1)
	if err != nil {
		successful = float64(0)
	}

	ch <- prometheus.MustNewConstMetric(
		KatranUpDesc,
		prometheus.CounterValue,
		successful,
	)

	if err != nil {
		return
	}

	for vip, packets := range packetsByVip {
		ch <- prometheus.MustNewConstMetric(
			KatranPacketsDesc,
			prometheus.CounterValue,
			packets,
			vip,
		)
	}
	for vip, bytes := range bytesByVip {
		ch <- prometheus.MustNewConstMetric(
			KatranBytesDesc,
			prometheus.CounterValue,
			bytes,
			vip,
		)
	}
}
