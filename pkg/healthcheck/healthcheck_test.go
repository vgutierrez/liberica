package healthcheck

import (
	"context"
	"testing"
	"time"
)

type TestHealthcheck struct {
	singleCheckCalledTimes int
	closedCalledTimes      int
}

func (thc *TestHealthcheck) GetCheckPeriod() time.Duration {
	return 1 * time.Millisecond
}

func (thc *TestHealthcheck) Close() {
	thc.closedCalledTimes++
}

func (thc *TestHealthcheck) SingleCheck() Result {
	thc.singleCheckCalledTimes++
	return Result{
		Successful: false,
		Mark:       0,
	}
}

func TestCheck(t *testing.T) {
	hc := &TestHealthcheck{}
	timeout := 20 * time.Millisecond
	ctx, _ := context.WithTimeout(context.TODO(), timeout)
	c := check(ctx, hc)
	want := Result{Successful: false, Mark: 0}
	// this will loop till the channel is closed, if it isn't properly closed
	// this test won't end
	for result := range c {
		if result != want {
			t.Errorf("Unexpected result: %v\n", result)
		}
	}
	if hc.singleCheckCalledTimes < 9 {
		t.Errorf("Unexpected number of executions: %v\n", hc.singleCheckCalledTimes)
	}
	if hc.closedCalledTimes < 1 {
		t.Error("Close() hasn't been called")
	}
}
