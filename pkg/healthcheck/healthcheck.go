/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"context"
	"sync"
	"time"
)

type Healthcheck interface {
	SingleCheck() Result
	Close()
	GetCheckPeriod() time.Duration
}

type Result struct {
	Successful bool
	Mark       int
}

// Start healthchecking several Healthchecks. Ensures that the output channel
// is properly closed after all goroutines have finished writing on it
func StartHealthchecking(ctx context.Context, hcs []Healthcheck) <-chan Result {
	var wg sync.WaitGroup
	c := make(chan Result)

	wg.Add(len(hcs))
	for _, hc := range hcs {
		go func(hc Healthcheck) {
			defer wg.Done()
			checkChan := check(ctx, hc)
			for {
				select {
				case result := <-checkChan:
					c <- result
				case <-ctx.Done():
					return
				}
			}
		}(hc)
	}

	// Clean up after we are done
	go func() {
		// Waits till all the goroutines writing on c have finished
		wg.Wait()
		close(c)
	}()
	return c
}

// Trigger a healthcheck periodically
func check(ctx context.Context, hc Healthcheck) <-chan Result {
	c := make(chan Result)
	ticker := time.NewTicker(hc.GetCheckPeriod())
	go func() {
		defer ticker.Stop()
		defer hc.Close()
		defer close(c)

		// issue a check immediately
		c <- hc.SingleCheck()
		for {
			select {
			case <-ticker.C:
				c <- hc.SingleCheck()
			case <-ctx.Done():
				return
			}
		}
	}()
	return c
}
