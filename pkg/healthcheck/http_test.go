/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"
	"time"
)

func testHttpHeaderValue(t *testing.T, headerName string, header map[string][]string, expected []string) {
	got := header[headerName]

	if len(got) != len(expected) {
		t.Errorf("Unexpected amount of HTTP Header %v occurrences: %v VS %v\n", headerName, len(got), len(expected))
	}
	for i, v := range got {
		if v != expected[i] {
			t.Errorf("Unexpected %v value: %v VS %v\n", headerName, v, expected[i])
		}
	}
}

func TestHTTPCheck(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte("OK"))
		testHttpHeaderValue(t, "User-Agent", req.Header, []string{UserAgent})
		testHttpHeaderValue(t, "Connection", req.Header, []string{"close"})
	}))
	defer server.Close()

	var tests = []struct {
		statusCode int
		want       Result
	}{
		{200, Result{Mark: 0, Successful: true}},
		{201, Result{Mark: 0, Successful: false}},
	}

	url, _ := url.Parse(server.URL)

	for _, tt := range tests {
		testname := strconv.Itoa(tt.statusCode)
		t.Run(testname, func(t *testing.T) {
			hc := NewHTTPCheck("test", server.URL, url.Host, 1*time.Second, tt.statusCode, 0)
			hc.CheckPeriod = 1 * time.Second
			ctx, cancel := context.WithCancel(context.TODO())
			c := check(ctx, hc)
			result := <-c
			cancel()
			if result != tt.want {
				t.Errorf("got %v, want %v", result, tt.want)
			}

		})
	}
}

func TestHTTPCheckTimeout(t *testing.T) {
	serverLag := 200 * time.Millisecond
	clientTimeout := 100 * time.Millisecond

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte("OK"))
		// Slow server
		time.Sleep(serverLag)
	}))

	var tests = []struct {
		timeout time.Duration
		want    Result
	}{
		{clientTimeout, Result{Mark: 0, Successful: false}},
		{serverLag + (10 * time.Millisecond), Result{Mark: 0, Successful: true}},
	}

	url, _ := url.Parse(server.URL)
	for _, tt := range tests {
		testname := fmt.Sprintf("%v", tt.timeout)
		t.Run(testname, func(t *testing.T) {
			hc := NewHTTPCheck("test", server.URL, url.Host, tt.timeout, 200, 0)
			hc.CheckPeriod = 1 * time.Second
			ctx, cancel := context.WithCancel(context.TODO())
			c := check(ctx, hc)
			result := <-c
			cancel()
			if result != tt.want {
				t.Errorf("got %v, want %v", result, tt.want)
			}
		})
	}

	// test TCP connection timeout as well
	server.Close()
	hc := NewHTTPCheck("test", server.URL, url.Host, clientTimeout, 200, 0)
	ctx, _ := context.WithTimeout(context.TODO(), clientTimeout*2)
	c := check(ctx, hc)
	for {
		select {
		case check := <-c:
			if check.Successful != false {
				t.Error("Unexpected test result")
			}
			return
		case <-ctx.Done():
			t.Error("check should have finished earlier than this")
			return
		}
	}
}

func TestHTTPS(t *testing.T) {
	server := httptest.NewTLSServer(http.HandlerFunc(func(rw http.ResponseWriter, _ *http.Request) {
		rw.Write([]byte("OK"))
	}))
	defer server.Close()

	url, _ := url.Parse(server.URL)
	hc := NewHTTPCheck("test", server.URL, url.Hostname(), 1*time.Second, 200, 0)
	hc.CheckPeriod = 1 * time.Second

	// inject test server RootCA into client to avoid certificate errors
	transport := server.Client().Transport.(*http.Transport)
	clientConfig := transport.TLSClientConfig
	clientTransport := hc.client.Transport.(*http.Transport)
	clientTransport.TLSClientConfig.RootCAs = clientConfig.RootCAs
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	c := check(ctx, hc)
	if !(<-c).Successful {
		t.Error("Unexpected HTTPCheck failure")
	}
}
