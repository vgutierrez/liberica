/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"errors"
	"fmt"
	"io"
	"net"
	"strconv"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"wikimedia.org/operations/software/liberica/pkg/metrics"
)

type ConnectionStatus int

const (
	Disconnected ConnectionStatus = iota
	Connected
)

// String used to identify the IdleTCPConnection Healthcheck
const IdleTCPConnectionHealthcheckName = "IdleTCPConnectionCheck"

type IdleTCPConnectionCheck struct {
	serviceID       string
	address         string
	d               net.Dialer
	conn            net.Conn
	status          ConnectionStatus
	lastAttempt     time.Time
	attempts        uint
	reconnectPeriod time.Duration
	mark            int
	CheckPeriod     time.Duration
}

func (itc *IdleTCPConnectionCheck) String() string {
	return fmt.Sprintf("IdleConnectionCheck: %s/%s", itc.serviceID, itc.address)
}

func (itc *IdleTCPConnectionCheck) GetCheckPeriod() time.Duration {
	return itc.CheckPeriod
}

func NewIdleTCPConnectionCheck(serviceID, address string, connectTimeout, reconnectPeriod time.Duration, mark int) *IdleTCPConnectionCheck {
	return &IdleTCPConnectionCheck{
		serviceID: serviceID,
		address:   address,
		d: net.Dialer{Timeout: connectTimeout,
			Control: func(network, address string, rawc syscall.RawConn) error {
				var fdErr error
				ctl := func(fd uintptr) {
					if mark != 0 {
						fdErr = setSocketMark(int(fd), mark)
					}
				}
				if err := rawc.Control(ctl); err != nil {
					return err
				}
				return fdErr
			},
		},
		status:          Disconnected,
		attempts:        0,
		reconnectPeriod: reconnectPeriod,
		mark:            mark,
		CheckPeriod:     300 * time.Millisecond,
	}
}

func (itc *IdleTCPConnectionCheck) updateMetrics() {
	m := strconv.Itoa(itc.mark)
	result := 0.0
	r := "down"
	if itc.healthy() {
		result = 1.0
		r = "up"
	}

	metrics.HealthcheckResult.With(prometheus.Labels{
		"service":     itc.serviceID,
		"healthcheck": IdleTCPConnectionHealthcheckName,
		"mark":        m}).Set(result)
	metrics.HealthcheckExecutions.With(prometheus.Labels{
		"service":     itc.serviceID,
		"healthcheck": IdleTCPConnectionHealthcheckName,
		"result":      r,
		"mark":        m}).Inc()
}

func (itc *IdleTCPConnectionCheck) connectionClosed() bool {
	one := make([]byte, 1)
	_, err := itc.conn.Read(one)
	if errors.Is(err, io.EOF) {
		itc.status = Disconnected
		itc.conn.Close()
		return true
	}

	return false
}

func (itc *IdleTCPConnectionCheck) SingleCheck() Result {
	if itc.status == Disconnected {
		itc.connect()
	} else {
		closed := itc.connectionClosed()
		// immediately reconnect to te be able to update the monitor status
		// to failed in one singleCheck() call if the service is actually down
		if closed {
			itc.connect()
		}
	}
	defer itc.updateMetrics()

	return Result{Mark: itc.mark,
		Successful: itc.healthy()}
}

func (itc *IdleTCPConnectionCheck) connect() error {
	// Attempt to reconnect immediately just once
	if itc.attempts >= 1 && time.Since(itc.lastAttempt) < itc.reconnectPeriod {
		time.Sleep(itc.reconnectPeriod)
	}

	var err error
	itc.attempts++
	itc.lastAttempt = time.Now()
	itc.conn, err = itc.d.Dial("tcp", itc.address)
	if err != nil {
		return err
	}

	itc.conn.(*net.TCPConn).SetKeepAlive(true)
	itc.conn.(*net.TCPConn).SetKeepAlivePeriod(15 * time.Second)
	itc.status = Connected
	itc.attempts = 0
	return nil
}

func (itc *IdleTCPConnectionCheck) healthy() bool {
	// Healthcheck will report an unhealthy endpoint iff
	// it is unable to reconnect after detecting a closed connection
	if itc.attempts >= 1 && itc.status == Disconnected {
		return false
	}

	return true
}

func (itc *IdleTCPConnectionCheck) Close() {
	if itc.status == Connected {
		itc.conn.Close()
	}
}
