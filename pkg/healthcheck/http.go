/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"crypto/tls"
	"net"
	"net/http"
	"strconv"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"wikimedia.org/operations/software/liberica/pkg/metrics"
)

// User-Agent header sent on every request performed by the HTTP(s) healthcheck
const UserAgent = "liberica/0.0.1 (https://wikitech.wikimedia.org/wiki/liberica; sre-traffic@wikimedia.org)"

// String used to identify the HTTP Healthcheck
const HTTPHealthcheckName = "HTTPCheck"

type HTTPCheck struct {
	serviceID          string
	url                string
	hostHeader         string
	client             *http.Client
	expectedStatusCode int
	mark               int
	CheckPeriod        time.Duration
}

// Do not follow redirects
func checkRedirectFunc(_ *http.Request, _ []*http.Request) error {
	return http.ErrUseLastResponse
}

func (hc *HTTPCheck) GetCheckPeriod() time.Duration {
	return hc.CheckPeriod
}

// timeout includes connection time + any redirects + reading the response body
func NewHTTPCheck(serviceID, url, hostHeader string, timeout time.Duration, expectedStatusCode, mark int) *HTTPCheck {

	tr := &http.Transport{
		DisableKeepAlives:  true,
		DisableCompression: true,
		TLSClientConfig:    &tls.Config{ServerName: hostHeader},
		DialContext: (&net.Dialer{
			Control: func(network, address string, rawc syscall.RawConn) error {
				var fdErr error
				ctl := func(fd uintptr) {
					if mark != 0 {
						fdErr = setSocketMark(int(fd), mark)
					}
				}
				if err := rawc.Control(ctl); err != nil {
					return err
				}
				return fdErr
			},
		}).DialContext,
	}

	return &HTTPCheck{
		client: &http.Client{
			CheckRedirect: checkRedirectFunc,
			Timeout:       timeout,
			Transport:     tr,
		},
		expectedStatusCode: expectedStatusCode,
		serviceID:          serviceID,
		url:                url,
		hostHeader:         hostHeader,
		mark:               mark,
		CheckPeriod:        1 * time.Second,
	}
}

func (hc *HTTPCheck) updateMetrics(statusCode int, d time.Duration) {
	s := strconv.Itoa(statusCode)
	m := strconv.Itoa(hc.mark)
	result := 0.0
	r := "down"
	if statusCode == hc.expectedStatusCode {
		result = 1.0
		r = "up"
	}

	metrics.HttpCheck.With(prometheus.Labels{
		"status_code": s,
		"service":     hc.serviceID,
		"url":         hc.url,
		"mark":        m}).Inc()
	metrics.HttpCheckSeconds.With(prometheus.Labels{
		"status_code": s,
		"service":     hc.serviceID,
		"url":         hc.url,
		"mark":        m}).Add(float64(d) / float64(time.Second))
	metrics.HealthcheckResult.With(prometheus.Labels{
		"service":     hc.serviceID,
		"healthcheck": HTTPHealthcheckName,
		"mark":        m}).Set(result)
	metrics.HealthcheckExecutions.With(prometheus.Labels{
		"service":     hc.serviceID,
		"healthcheck": HTTPHealthcheckName,
		"result":      r,
		"mark":        m}).Inc()
}

func (hc *HTTPCheck) SingleCheck() Result {
	req, err := http.NewRequest("GET", hc.url, nil)
	result := Result{Mark: hc.mark, Successful: false}
	if err != nil {
		return result
	}
	req.Host = hc.hostHeader
	req.Header.Set("User-Agent", UserAgent)
	start := time.Now()
	resp, err := hc.client.Do(req)
	elapsedTime := time.Since(start)
	if err != nil {
		// Metrics should be updated when a HTTP response isn't received too
		hc.updateMetrics(0, elapsedTime)
		return result
	}
	defer resp.Body.Close()
	defer hc.updateMetrics(resp.StatusCode, elapsedTime)

	result.Successful = resp.StatusCode == hc.expectedStatusCode
	return result
}

func (hc *HTTPCheck) Close() {}
