/*
Copyright © 2022 Valentin Gutierrez <vgutierrez@wikimedia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package healthcheck

import (
	"context"
	"fmt"
	"net"
	"testing"
	"time"
)

func handleConnections(t *testing.T, l net.Listener, closeAfter time.Duration) {
	for {
		conn, err := l.Accept()
		if err != nil {
			return
		}
		go func(c net.Conn) {
			time.Sleep(closeAfter)
			c.Close()
		}(conn)
	}
}

func TestIdleTCPConnectionCheck(t *testing.T) {
	var tests = []struct {
		closeConnectionAfter time.Duration
	}{
		{5 * time.Second},
		{20 * time.Millisecond},
	}

	for _, tt := range tests {
		testname := fmt.Sprintf("%v", tt.closeConnectionAfter)
		t.Run(testname, func(t *testing.T) {
			l, err := net.Listen("tcp", ":0")
			if err != nil {
				t.Fatalf("Unable to listen: %v\n", err)
			}
			go handleConnections(t, l, tt.closeConnectionAfter)

			reconnectPeriod := 10 * time.Millisecond

			hc := NewIdleTCPConnectionCheck("test", l.Addr().String(), 100*time.Millisecond, reconnectPeriod, 0)
			hc.CheckPeriod = 10 * time.Millisecond
			ctx, cancel := context.WithCancel(context.TODO())
			defer cancel()

			c := check(ctx, hc)
			timer := time.NewTimer(10 * reconnectPeriod)
			stopServerTimer := time.NewTimer(5 * reconnectPeriod)
			serverRunning := true
			var serverStoppedAt time.Time
			want := Result{Mark: 0, Successful: true}

			for {
				select {
				case check := <-c:
					if !serverRunning && time.Since(serverStoppedAt) >= reconnectPeriod {
						want.Successful = false
					}
					if check != want {
						t.Errorf("Unexpected healthcheck result, got %v want %v", check, want)
					}
				case <-stopServerTimer.C:
					l.Close()
					serverStoppedAt = time.Now()
					serverRunning = false
				case <-timer.C:
					return
				}
			}
		})
	}
}
