/*
Copyright © 2023 Valentin Gutierrez <vgutierrez@wikiemdia.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"wikimedia.org/operations/software/liberica/pkg/metrics"
)

// katranExporterCmd represents the katranExporter command
var katranExporterCmd = &cobra.Command{
	Use:   "katranExporter",
	Short: "Expose basic katran metrics",
	Run: func(cmd *cobra.Command, args []string) {
		reg := prometheus.NewPedanticRegistry()

		// Add the standard process and Go metrics to the custom registry
		reg.MustRegister(
			metrics.NewKatranMetricCollector("127.0.0.1:50051"),
			prometheus.NewProcessCollector(prometheus.ProcessCollectorOpts{}),
			prometheus.NewGoCollector(),
		)
		http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
		log.Fatal(http.ListenAndServe(":2113", nil))
	},
}

func init() {
	rootCmd.AddCommand(katranExporterCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// katranExporterCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// katranExporterCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
